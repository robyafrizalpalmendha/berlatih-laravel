<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function homepage() {
        // return "Selmat Datang";
        return view('page.home');
    }
}
