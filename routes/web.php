<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@homepage');
Route::get('/register', 'AuthController@register');
Route::post('/welcomes', 'AuthController@welcomes');


Route::get('/home', function() {
    return view('items.index');
});

Route::get('/data-table', function() {
    return view('items.datatable');
});

Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan/{id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{id}', 'PertanyaanController@destroy');


// Bisa juga parameter id ditambahkan parentnya dari database nya :
// Route::get('/pertanyaan/{pertanyaan.id}', 'PertanyaanController@show');
// Route::get('/pertanyaan/{pertanyaan.id}/edit', 'PertanyaanController@edit');
// Route::put('/pertanyaan/{pertanyaan.id}', 'PertanyaanController@update');
// Route::delete('/pertanyaan/{pertanyaan.id}', 'PertanyaanController@destroy');