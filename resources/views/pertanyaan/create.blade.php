@extends('adminLTE.master')

@section('title')
    <span>Create Pertanyaan</span>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8 ml-5 mt-3">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Title</label>
                    <input type="text" class="form-control @error('judul') is-invalid @enderror" id="judul" name="judul" value="{{ old('judul', '') }}" placeholder="Enter Title">
                    @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <!-- </div>
                  <div class="form-group">
                    <label for="isi">Content</label>
                    <input type="text" class="form-control @error('isi') is-invalid @enderror" id="isi" name="isi" value="{{ old('isi', '') }}" placeholder="Enter Content">
                    @error('isi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  //Bisa juga menggunakan Textarea -->
                  <div class="form-group">
                    <label for="isi">Content</label>
                    <textarea class="form-control @error('isi') is-invalid @enderror" id="isi" name="isi" placeholder="Enter Content" cols="30" rows="10">{{ old('isi', '') }}</textarea>
                    @error('isi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
@endsection