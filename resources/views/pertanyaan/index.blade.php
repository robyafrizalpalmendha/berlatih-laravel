@extends('adminLTE.master')

@section('title')
    <span>Index Pertanyaan</span>
@endsection

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">CRUD</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                    </div>
                    @endif
                <a class="btn btn-primary mb-4" href="/pertanyaan/create">Create New Pertanyaan</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Title</th>
                      <th>Body</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <!-- @foreach($pertanyaan as $key => $tanya)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$tanya->judul}}</td>
                            <td>{{$tanya->isi}}</td>
                            <td style="display: flex;">
                                <form action="/pertanyaan/{{$tanya->id}}" method="POST">
                                    <a class="btn btn-info btn-sm mr-2" href="/pertanyaan/{{$tanya->id}}">Show</a>
                                    <a class="btn btn-secondary btn-sm mr-2" href="/pertanyaan/{{$tanya->id}}/edit">Edit</a>
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @endforeach -->

                    <!-- Atau pengulangan cara lain ketika data kosong menampilkan keterangan -->
                    @forelse($pertanyaan as $key => $tanya)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$tanya->judul}}</td>
                            <td>{{$tanya->isi}}</td>
                            <td style="display: flex;">
                                <a class="btn btn-info btn-sm mr-2" href="/pertanyaan/{{$tanya->id}}">Show</a>
                                <a class="btn btn-secondary btn-sm mr-2" href="/pertanyaan/{{$tanya->id}}/edit">Edit</a>
                                <form action="/pertanyaan/{{$tanya->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center">No Data</td>    
                        </tr>
                    @endforelse


                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
           </div>
        </div>
@endsection