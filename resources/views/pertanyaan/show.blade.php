@extends('adminLTE.master')

@section('title')
    <span>Show Pertanyaan</span>
@endsection

@section('content')
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">{{$tanya->judul}}</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
          {{$tanya->isi}}
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <a href="/pertanyaan" class="btn btn-warning">Back</a>
        </div>
      </div>
@endsection