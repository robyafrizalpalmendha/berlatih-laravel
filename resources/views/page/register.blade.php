<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form Sign Up</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcomes" method="POST">
        @csrf
      <label for="firstname">First name:</label><br /><br />
      <input type="text" id="firstname" name="firstname" required /><br /><br />

      <label for="lastname">Last name:</label><br /><br />
      <input type="text" id="lastname" name="lastname" required /><br /><br />

      <label>Gender:</label><br /><br />
      <input type="radio" id="male" value="male" name="gender" />
      <label for="male">Male</label><br />
      <input type="radio" id="female" value="female" name="gender" />
      <label for="female">Female</label><br />
      <input type="radio" id="others" value="others" name="gender" />
      <label for="others">Other</label><br /><br />

      <label for="nationality">Nationality:</label><br /><br />
      <select name="nationality" id="nationality" required>
        <option value="Indonesian">Indonesian</option>
        <option value="Chinese">Chinese</option>
        <option value="English">English</option>
        <option value="Indian">Indian</option>
        <option value="Malaysian">Malaysian</option></select
      ><br /><br />

      <label>Language Spoken:</label><br /><br />
      <input
        type="checkbox"
        id="indonesia"
        value="indonesia"
        name="indonesia"
      />
      <label for="indonesia">Bahasa Indonesia</label><br />
      <input type="checkbox" id="english" value="english" name="english" />
      <label for="English">English</label><br />
      <input type="checkbox" id="other" value="other" name="other" />
      <label for="other">Other</label><br /><br /><br />

      <label for="bio">Bio:</label><br /><br />
      <textarea name="bio" id="bio" cols="30" rows="10" required></textarea
      ><br />

      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
